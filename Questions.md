### 1. CSS-Modules (Ч.1 П.8) - верно ли, что не будет возможности из родителя переопределять стили дочерних компонент? [link](https://github.com/css-modules/css-modules/issues/167)
Примеры использования:
* Места использования switchNumber (например, компоненты buyProduct, mini-/mobile-/desktop- cartItem)  
![switchNumberUsage](switchNumberUsage.PNG)
* bonusValue в меню аккаунта в header'е страницы (кастомные паддинги)  
![bonusValueCustomized](bonusValueCustomized.PNG)

### 2. Передача в дочерние компоненты объекты, а не поля объектов (Анти. П.1)

**Плюсы подключения объектов по полям:**
* Валидация данных - наличие и верный тип <или перепроверка в каждом месте использования>
* Задание значения по умолчанию <или тернарный в каждом месте использования>
* Универсализация компонента (происходит маппинг данных при подключении в другом компоненте) <или под каждую структуру с отличным названием поля в объекте - делать компонент адаптер>

**Возможные меры:**
* Всегда производить внутренний маппинг приходящих объектов в props, через написание соответствующего `validatedObject` в `computed` секции
* Для объектов писать функции валидаторы в props (не закрывает проблему отсутствия значений по умолчанию) [link](https://vuejs.org/v2/guide/components.html#Prop-Validation)

Пример:  
**_DepartmentCategoriesList**
```html
<template lang="pug">
    CssModule: .root
        ul.list
            _DepartmentCategoriesListItem.item(
                v-for="item in items"
                :key="item.id"
                :item="item"
            )
</template>
```
**_DepartmentCategoriesListItem**
```html
<template lang="pug">
    CssModule: .root
            li.card
                img.photo(
                    :src="validatedItem.imageUrl"
                )
                a.link(
                    :href="validatedItem.href"
                    @click="onCategoryClick"
                ) {{validatedItem.title}}
</template>
```
```javascript
<script>
    import { isDefined } from '@/scripts/tools/helpers.js';
    import logger from '@/scripts/tools/logger.js';

    export default {
        name: 'DepartmentCategoriesListItem',
        components: { ...$autorequire },
        props: {
            item: Object
        },
        computed: {
            validatedItem() {
                let localItem = this.item;

                if (!isDefined(localItem)) {
                    logger.error(`${this.$options.name}: Prop 'item' must be defined`);
                    localItem = {};
                }

                if (!isDefined(localItem.title)) {
                    logger.warn(`${this.$options.name}: Prop 'item' doesn't have title`);
                    localItem.title = '';
                }

                if (!isDefined(localItem.href)) {
                    logger.warn(`${this.$options.name}: Prop 'item' doesn't have href`);
                    localItem.href = '';
                }

                if (!isDefined(localItem.imageUrl)) {
                    logger.warn(`${this.$options.name}: Prop 'item' doesn't have imageUrl`);
                    localItem.imageUrl = '';
                }

                return localItem;
            }
        },
        methods: {
            onCategoryClick() {

            }
        }
    };
</script>
```

### 3. Отказ от маппинга данных с бекенда (Анти. П.1)

**Плюсы маппинга данных с бекенда:**
* Валидация данных - наличие и верный тип <или перепроверка в каждом месте использования>
* Задание значения по умолчанию <или тернарный в каждом месте использования>
* В месте маппинга генерация необходимых для FE полей, на основе полученный, не присылаемых с бекенда (пример, флаг `isUseBPG` в `cartService`)

![BE FE data transfering](BE-FE-data-transfer.PNG)

**Возможные меры:**
* Делать маппинг, при этом всегда делать его одноименно полям пришедшим с бекенда

### 4. Верно ли, что при отсутствии маппинга данных с бекенда и проверок/конвертаций на пути к местам использования в компонентах - мы в каждом месте использования должны будем писать проверку?
Например:
```
<template lang="pug">
    CssModule: .root
        .card
            a.link(
                :href="category.href"
            ) {{category.name ? category.name : ''}}
                img.photo(:alt="category.name ? category.name : ''")
</template>
```

### 5. Порядок опций компонента
```javascript
<script>
    export default {
        el: '#dummy',
        name: 'DepartmentCategoriesList',
        parent: 'dummy',
        functional: 'dummy',
        delimiters: 'dummy',
        comments: 'dummy',
        components: {
            ...$autorequire
        },
        directives: 'dummy',
        filters: 'dummy',
        extends: 'dummy',
        mixins: 'dummy',
        inheritAttrs: 'dummy',
        model: 'dummy',
        props: {},
        data() {
            return {};
        },
        computed: {},
        watch: {},
        beforeCreate() {},
        created() {},
        beforeMount() {},
        mounted() {},
        beforeUpdate() {},
        updated() {},
        beforeDestroy() {},
        destroyed() {},
        methods: {}
    };
</script>
```

### 6. Порядок атрибутов элемента
```pug
<template lang="pug">
    Footer(
        is=""
        v-for=""
        v-if=""
        v-show=""
        v-cloak=""
        v-pre=""
        v-once=""
        id=""
        ref=""
        key=""
        slot=""
        v-model=""
        
        :src=""
        href=""
        type=""
        otherHtmlNativeAttrs=""
        :andNonHtmlNativeAttrs=""
        
        v-html=""
        v-text=""
        @click=""
        @userAction=""
    )
</template>
```

### 7. Роутинг и структура проекта
![prj-structure](prj-structure.png)
